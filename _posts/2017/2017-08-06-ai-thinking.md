---
layout:     post
title:      如何用机器学习和知识图谱来实现商业智能化？
category:   [机器学习]
description: 机器学习
keywords:  机器学习
---

## 如何用机器学习和知识图谱来实现商业智能化？

2017-08-06 Richard 8爪鱼作坊

  > 7 月22 - 23 日，由中国人工智能学会、阿里巴巴集团 & 蚂蚁金服主办，CSDN、中国科学院自动化研究所承办的第三届中国人工智能大会（CCAI 2017）在杭州国际会议中心盛大开幕。



    大会第二天，德国人工智能研究中心（DFKI）科技总监、北京人工智能技术中心（AITC）总监兼首席
    科学家Hans Uszkoreit发表了主题为《机器学习在商务智能中的创新应用》的演讲。
    Hans Uszkoreit介绍了如何分析各种来源的数据，用于执行众多的商务智能任务，如供应链监控、
    市场调研和产品管理等。其所讨论的方法包含了不同类型的机器学习和基于知识的自然语言理解技术，
    充分利用知识图谱和各种其他结构化数据源，实现信息的互为补充。


  以下是Hans Uszkoreit的演讲全文，AI科技大本营略做修改：

        今天我将介绍目前人工智能的两个主要方向，基于行为的学习和基于知识的学习；另外我会讲一下商业智能以及工业4.0、开放数据与企业数据，以及开放的知识图谱和企业知识图谱；接着我会介绍文本分析的大数据方法、文本数据理解中的机器学习和结构化知识；
        最后我会讲一下机器学习机器前景，这个大家已经看到很多了，两种不同的系统
        我们看到，有很多的新闻都在报道人工智能在深度学习上所取得的一些成功，这已经听说过很多了。这些成果涉及人工智能各个方面，如语音、文本和自动驾驶等，深度学习似乎正在改变我们的生活，确实也是如此。
        但我们还有另外一种系统，这就是IBM的Watson，它在一个美国很著名的综艺节目里面获胜了，他们没有进行任何深度学习，它只是IBM系统的成功。Wason是另外一种系统，它可以掌握大量的结构化的知识，将非结构化知识当作结构化知识使用。


我们看到有两种不同的系统：在人工智能历史上很多系统都是基于知识的系统，有一种系统更多的是针对比较小群体的行为，比如说使用基于规则的专家系统来检查信用的，且已经用了很多年了；还有一种系统，在进行机器学习之前做研究行为，我们见到的更多是经典的反应性机器学习。

后来在90年代的时候，机器学习崛起，并在2000年之后变得更加的成功，也有很多的分类还有方法，我们现在的话在两侧都有机器学习。比如像谷歌的机器翻译，还有很多新的系统是用于自动驾驶，另外还有语音理解。

所以说，像人工翻译只能理解它能够理解的东西，但是谷歌的某些机器翻译是像鹦鹉学舌一样进行学习，他们学习特定的行为。但是，它们对语言本身没有任何理解，它们本身也没有的固有知识，有的只是隐性知识。因此它们无法理解中文或中文的属性。

另外，我们还有一种是IBM的Watson和聊天机器人，它们需要控制大量的结构化知识，而且这些知识是动态的，将这些知识放到深度学习多层神经网络中并不是优选的策略。DFKI也是这样一种系统。

未来我们会开发出一些可以作预测的超人类人工智能，可以更好地结合两者，这是我们接下来的重要一步。我刚才已经说过这些系统没有真正的知识，所以说我们把它叫做“狭义人工智能”。比如，系统不能做些其他系统的任务：能下围棋的系统不能做翻译，能翻译的系统不能做驾驶，能驾驶的系统不能做翻译。

机器可以模仿和学习人类的行为，比如说学习世界顶级棋手的下棋方式；我们驾驶的系统可以从人的驾驶行为中学习，并能避免发生事故；翻译的机器人可以同时翻译几十种不同的语言，人是做不到这一点的。

但是机器还是无法模仿四岁儿童在一小时内的行为。四岁儿童懂得的知识虽少，但他所有这些知识是可以重新再利用得，比如说他可以从冰箱里取出东西或放回去东西，他也能够回答关于冰箱的一些知识。目前我们的深度学习没有这种可再利用的知识。


拿生活当中的人类医生来说，医生在一生当要不断学习很多知识，因为医学在不断地进步和变化。也许经过一段时间的发展，医生看到患者身上出现多种症状的组合或生命体征的组合，他们有时会对化验结果无法作出解释。有很多人认为中医很神奇，他们可以根据病人症状作出诊断，有很多的医生是有这种直觉的，这种直觉可以帮你找到答案。这些答案虽然有可能是正确的，但是医生不能够解释疾病根源。

我们知道深度学习也是这样，知识的应用完全是由直觉而不是知识驱动的。区别就是要解决知识上挑战的问题，人们需要理解。没有理解的话我们不可能解决问题，其实如果能理解，你可能就已经找到问题的解决方案。很多的问题我们无法解决就是因为我们不理解问题，但是深度学习系统不同，它们没有任何理解，完全依靠直觉解决问题。

基于人工智能的商业分析应用

接下来我要讲的是应用的部分：人工智能商业分析。大家当中有很多人从事这个领域，以下是这方面的一些目标：

过程监督，比如说像生产、物流等；然后是偏差分析；
决策辅助措施选择。大部分决策由人类的决策者作出，但是有一些决策时可以自动生成；
为流程优化提供方案。这不是由人类进行的流程优化；
预测性分析。用以协助作出预测和规划，以及对半自动控制的预测性分析。在生产方面不需要人就可以进行优化的情况很少，比如说物流、机器部件的运动，材料的高效利用等，如果要做长期规划的话，还是需要由人类完成。

但是，经常会有人问一个问题：商业分析和工业4.0之间有什么区别？

大家可能在中国听说过工业4.0，现在非常流行，但是对这方面的讨论也许并不深入。

在德国我们进行了很多讨论，因为这个词就来自于德国，其实是DFKI（德国人工智能研究所）的CEO也就是我的老板提出的。我是在柏林中心，我们的老板是负责5个中心。他找了两个人，一个来自于行业，一个来自政府，这个词就是是他们想出，他们觉得这是第四次工业革命。第三次是计算机革命，而第四次工业革命就是通过物联网把所有的机器紧密连接在一起。我们要创造的是完全数字化的企业，不止是有互联，同时是完全数字化的工厂。我们等会儿来看看这两个东西怎么样进行结合。

这边有两个词是我经常会提到的。

另外的话还有智能工厂，这是工业4.0的一个概念，里面包含了很多组成部分。在智能工厂里面，所有的设备、产品都是通过物联网进行连接。所有生产是通过产品记忆来进行操作，产品在进行周转时，机器会告诉它要做些什么。无需对机器进行重新编程，机器通过产品学习。产品周转到一个设备之后，机器会告诉设备要对它进行什么操作，所有这些流程都是通过协作来完成的。比如说一个产品来了之后可以等待一段时间，这是通过技术互联来实现的。

另外我们还有智能移动、智能物流、智能电网、智能建筑，所有这些结合在一起形成一个空间。实际上商业分析就是要收集智能工厂里的数据。现在对商业分析最为重要的是把企业内部和外部的数据结合起来。但是却被大多数人忽视，因为现在人们主要关注的是怎么样把企业本身进行数字化，以及怎么把生产、规划、物流等企业运作流程数字化，利用算法来和数据流进行管理。

但是对公司来说，最为重要的是公司以外的东西，为什么是这样呢？因为出钱的客户在公司外，供应商也是，甚至工人下班之后也要回家，也是就是到了公司外部。另外政策制定者、税务人员等都是在企业外部。所以智能的核心就是把这些内部的信息和外部的信息结合在一起，先是把外部的数据和内部的数据进行对接。比如说我们产品的一些功能不太受欢迎，我们就生产大量没有改功能，这样才能够适合外部的需求，这样的话就可以简化问题。很多东西都是基于外部的，接下来我们来看下一点。

现在我们正利用人工智能化进行企业内外数据的连接，现在这两类数据结合的还不是很好。总体而言，社会完成数字化转型也包含两部分内容。一部分是内部的东西，比如说网络物理系统、物联网、智能企业，另外还有开放领域的东西，比如语义网络、数据和知识社区。我们在企业内部谈论的是数据库或数据中心，企业通常有很多关于消费者产品、财务的数据，在外面的话是完全不同的数据库，比如维基百科等。这些数据库属于不同的世界，现在还没有结合在一起。


结构化的知识

我想说明一下图中的“小泡”，也就是是开放数据连接“小泡”。我不知道你们有多少人从事这个领域，我之后可能会介绍。外部有很多东西，公司内部也有一些其他的东西，我们都知道公共知识有很大的增长，我们利用它可以做很多事情。如果说我们把整个维基百科中的东西都印刷出来，就像印刷出版《百科全书》那样，那会是多么庞大的工作。

公共知识为什么对我们来说会如此重要？这些非结构化的数据以文本形式储存在图书馆，因为收集整理这些数据是迈向数据结构化的重要一步。另外我们还有知识图谱，比如谷歌将最早的免费知识图谱进行完善，然后免费将其回馈给社会，还有像Bing、百度等也在做这项工作，这跟建造工厂不是一回事。


如果说现在随机选择一个美国的搜索引擎进行搜索，我们会在搜索结果页面的右侧看到一些小框，它们并不来自于文件，左边是来自于文件，是典型搜索引擎的搜索结果。在右边我们看到的东西其实都是来自于知识图谱的非结构化知识。

现在有越来越多的团体和企业想要来做这样的一些知识图谱，我这边列出了一些。第一个Yago是在赛尔布鲁肯，非常有名。第二个是DBpedia，他们在欧洲做开放数据库，他们努力将很多领域的知识集中在一起。我们也跟他们有合作。Freebase还在但是大不如前，它已成为wikidata的一部分。大家可能都知道wikidata，当然也有些中国人并不知道，wikidata积极倡导将非结构化知识转化为结构化知识，它在此类项目中是最大的。Wikidata基金位于柏林，我们和他们在相关项目上有非常密切的合作。我在这里就不说Google Knowledge Vault了，因为它已不再那么干净了，其中部分或大部分都是自动收集的数据。



上面这张图其实有好几年的历史了，为什么没新的呢？这张照片的每一个小泡泡，都是一些基于数据的语意知识或者结构式知识库，其实一些像是Web 3.0，一些更像是语意网络，还有一些更像是数据库。但是其实它们在语意上面都是相互联系的，形成相互联系的开放数据。每个小泡上都至少有一种联系将其与其他小泡连接在一起，在这么多泡泡当中，你会看电影数据库、名人信息数据库，化学元素数据库等等。为什么已经过了好几年我们还在用这张图？因为现在这样的一张图没有办法再把其他这几年新的内容加进去，这张图已经容纳不下了。

我们把DBpedia的数据库放在中心，因为他们正在努力将其他的数据库连接起来。在我们自己的项目当中，我们做了一个尝试，希望能够在工业应用中将不同数据类型连接在一起，一些我们使用方法是和 DBpedia的方法相同，用以解决一些行业问题。



从这边我们可以看到，有一些比较特殊的一些数据，这些数据你只能和大公司合作才能获得，比如你在阿里、京东工作，或者是大型的物流企业，电信企业的话。但是右手边的数据就便宜的多，比如像气象学数据、媒体新闻数据、地理数据和卫星数据等，这些都非常容易获得。但是图最上方的是科学知识、知识社区（包括维基百科）、其他开放数据等，蓝色方框中的是企业内部的数据。如果将不同来源的数据整合在一起，就会带来巨大的价值。

如果我们要为某一地区开发一个运输分析APP，我们可能会用到交通数据和开源知识社区数据，后者会为你提供开放式街景图、场所、产品类型、包装等等一些信息。还有就是气象学数据，因为对运输而言，气象非常的重要。你还可以从物流公司获取地理数据和卫星数据。如果能够垂直整合这些数据的话，你就能够做出非常棒的产品。


为什么要重视非结构化数据

接下来我想简单的介绍一下大家可能听过的一些内容，如果大家不熟悉的话我想再说明一下为什么非结构化的数据能够扮演如此重要的角色。在商业分析的领域当中，人们希望能够借助时间序列、回归等等听起来很酷炫的机器学习办法来分析问题，这可能是一些销售噱头，但是这个不重要。



我想说的是，为什么图中会有这么多的上升和下降？企业使用诸如真实气象数据这样的数字数据，希望能找到更多的商业营销卖点，可以卖冰激凌也可以卖雨伞。但是获取再多这样的数据也无法解释一切事件。如果我们获取更多的数据，如新闻和推特数据，那么就能搞清为什么会出现某些高峰或低谷，并将它们与真实生活中发生的事件联系在一起，比如说是关于纽约新港港口和船只博览会的新闻和推特数据。



我们再来看一下医疗数据，医疗数据现在大多是数字数据和图像数据。但是如果没有医生的报告将相关的事件、发现和假设提取出来，这些数据就没多大用处。因此只有将非结构化数据和结构化数据结合起来，才能解决问题。这就是我们以及Wikidata、DBpedia现在希望完成的工作，我们尝试将信息提取出来并尽可能将其转换成结构化数据，将它们放在知识图谱中。

如果我们看一下文献信息提取，就会发现人们试图涵盖所有东西，比如说从某些对话（如客户关系管理）中提取主题和答案，再重复利用提取出的答案。我们还可以提取名称、事实、事件、意见和情绪等。现在我们有这样一个项目，我们在该项目和它的几个子项目中与很多先锋公司和大公司进行了合作，如西门子、莱比锡大学等等。还有一些外部的合作伙伴，像WIKIMEDIA、Wikidata基金等等。我们也和提供商业数据的公司进行了合作，如BBD柏林数据中心、WVC德国和奥地利分公司。

下面介绍一下我们在这个项目中所使用的方法：

将企业内部数据与开放数据、开放知识和新媒体（比如新闻媒体、电视、社交媒体）内容进行整合；
将知识图谱和开源办法和工具连接起来；
将数据分析整合到强大的大数据技术中；因为不久以后数据就会变得非常庞大，无法用常规的技术进行处理。
结合使用结构化方法、统计方法和深度学习分析异构数据；
为新型数据价值链构建可行商业模型和法律框架。在欧洲几乎一切成果都受严格的知识产权保护，同时也受数据隐私和数据安全方面的权利保护。因此我们与律师们进行合作，如果没有他们，我们就无法在欧洲完成如此庞大的工作。



我们从互联网上获得数据包括非结构化数据、半结构化数据以及企业内部数据。这张图其实是很典型的知识图谱，我们所做的就是要把不同来源的数据输入到这个知识图谱中，然后得出整合后的知识图谱，将其扩展为公共知识图谱，这基本就是这个项目的原理。



这个图就不赘述了。这是一个企业内部语义网，它定义了企业之间，企业与产品之间，产品和科技，科技和科技，企业内部人与人之间关系。最后就会得到一张这样的知识图谱，现在应用在供应链关系和市场调研中。

我们还在其他领域应用了相同的方法，但是从这个项目来看的话，更多是和应用有关的，这个是另外一个项目了，我们在其他项目中也有相关应用。我们使用的数据包括：交通数据、开源数据、媒体数据、采购数据等等，可以利用得其他数据还有很多，这已经足够多了。

我之前也跟大家谈到，这样的发展是非常迅速，外部的数据越来越多，你要把所有的数据嵌入到大数据，我所在的DFKI柏林中心的大数据部门的主任也成立了这样的一个小组，专门做了一个大数据的平台。我们可以使用其他的大数据平台，为什么要使用Apache Flink这个平台？不仅是因为他本身就是Apache Flink的开发者，而且还因为这个平台非常擅长于数据流处理。相比之下，Spark在流处理时只是将数据分成一个小批次进行处理，并不是真正的流处理。如果我们要进行实时数据的流处理的话，它就不如Flink，这就是为什么我们最后选择了Apache Flink。

我们将工具挖掘（mean crawling）外包给了一家公司，我们对语义工具进行过滤，然后再进行预处理、实体发掘和联系以及事件提取，最后得出可以用在多个项目中的KPI。接下来我们再谈谈实体联系，这项工作极具挑战性，这个系统是我们几年前构建的，并在2015年的时候获得一个奖，我们这几年一直在做改善该系统，效果也不错。比如说阿姆斯特朗这个名字，它可能是指摇滚乐手、宇航员等，我们将内部知识和外部知识联系起来，识别出来谁是你想查的那个人。微机百科也要使用，有的企业数据结构化，对于这样的信息我们也会整体在实体联系的结果当中。

方法论这块待会儿再来谈，这是一种混合优化和深度学习的方法，多目标优化在处理联系和歧义方面做的比深度学习好，在处理动词时，我们则使用深度学习。在消除动词时态歧义时，我们就使用的是深度学习，因为并没有太多上下文可以依据。对于数值优化，传统的一些办法效果更好，但是在这个情况下可能做不到那么好。

现在在动词时态消除歧义和实体联系上，几乎所有人都声称自己做的更好，但是从我们个人角度来说我们从来不说自己做的最好，我们在所专攻的领域上做的的非常好，其中一个领域是：进行关系提取以获得事实。我们希望学习关于公司的事实，比如说：哪一个供应商有问题，哪一个供应商濒临破产，哪一个供应商出现了罢工问题，以及哪家公司在发布新产品。在这方面，我们使用的是语言学最小限度远程监督机器学习(Minimally and Distant Supervised ML for Linguistic)。

和其他人一样，我们从语言处理监督学习研究人员先前提出的事实开始着手，最早的是斯坦福大学研究人员发表的论文。我们怎么做的？其实跟他们不太一样，我等会在向你们说明。我们获取了数千个事实，然后在谷歌搜索引擎上进行搜索，在得出的搜索结果网页中，把句子断句之后确定句子的相依性，然后通过复杂的模式提取机制进行提取，刚开始的时候这种机制并不理想，许多机构在这里败下来，因为这种机制通常只能适用于短句、断句的语意分析。我们使用了语义知识图谱，例如Wordnet，用其中的资源进行过滤以确保提取出的内容具有语义上的模式。



举一个例子，一个人跟另外一个人结婚，会有很多不同的模式，实际上有数千种不同的模式。为什么模式研究很重要？因为这些模式将我们与知识联系在一起的，我们现在已经把模式和语句进行了匹配，最后我们会得到一个模式图，其中橙色框中的内容是给出了我们希望找出的关系，然后我们再基于这么多的关系来进行统计分析，例如频率分析。



从上图可以看出，我们整个处理流程是一个闭环。从实例开始，进行最小限度监督学习，然后再输入搜索出来的更多实例，尤其是当你的实例达不到1万个时，你可以进行最小限度的监督学习。当你有了很多的实例之后，我们可以把实例不断的加进来，现在我们的系统可以把监督学习、无监督学习、最小限度的监督学习、远程监督学习整合起来。

很简单，你只需将带标签的陈述(tagged mentions)加到解析器(Parser)中，当你把这样的样本加进来之后，整个系统就会自动跑起来。如果是有标签样本，你可以能够去跑3次这样的循环，因为我们有一个监督式、非监督、远程监督的机器学习方法，可以做三次数据的分析。我们一开整合那么多的机器学习的方法，是因为一开始的数据是有限的。现在我们的系统在日以继夜地运转，过去6个月当中我们也得到了很多不同的数据，现在已经涵盖120万个企业的实体知识库，我们可以跟踪他们的信号，我们采集的只有收到语义信号的企业的数据。

当然了从某些角度来看，这些数据并不是干净、有序的，因为这是结构和模式的混合式。但是在应用中，我们需要对比各种方法。方法由很多：有限状态的方法(finite-state methods)，混合优化，多对象优化，卷积神经网络等等。

但是很遗憾的是，现在我们的目标是将成果应用到实际生活中，我们必须从每种方法中找到最适合我们数据的部分。有这么多方法，我们可以为深度学习领域或其他基于知识的英语中的从业人员提出很多不同的解决方案，从而实现早期应用。

 我们在一些领域已经有一些应用，比如说AI辅助产品经理，我们可以从全球的信息化企业搜索他们客户在想些什么；我们还可以通过人工智能进行供应链的管理；进行市场调研；辅助投资经理进行投资的管理。最后来看一下前景，我们有几分钟时间讲讲我们未来的前景，这就是我们的现实。我讲的这些都是我们是能够做到最好的，我不知道你们做的怎么样，我们正尽全力试图将成果应用到我们的企业中。我们必须总和这些方法，因为每一种方法都不够完美，比如说像机器学习、深度学习等。因此我们通过衡量每种方法挑选出当前最好的方案，但是未来会怎样？



上图是DFKI的CEO做的图，我前面有讲过工业4.0就是他的想法，他同时也是一名人工智能研究人员。到1975年的时候，我们现在主要的是一些搜索、启发式的搜索和认知的一种方法，到1995年是基于知识的系统，人们试图研究知识工程学并构建海量的知识，这非常困难，而且当时没有足够多的知识可以建立。

为什么太困难？有两个原因，一个是因为他们野心太大了，他们想添加太多逻辑；当时只有一个人想要建立这样的知识，现在我们的知识系统是由数百万人共同构建，所以说是非常不一样的。到2015年的时候我们有了学习系统，非常大，现在的话也很大。我写的是到2015年不代表到这里就停止了，我认为这个系统在中国发展的就非常迅速。



它的意思就是说，如果回溯50年或100年，当时我们认为下一个浪潮是系统整合，就是把各系统的功能整合在一起。我一开始的时候忘了讲，其实新系统有核心上不一样的东西。很多系统通过数据来进行学习，同时他们还需要一些时间运行额外的数据，它们或许不够稳定，或许是我们不知道怎么样进行选择，它们也有可能太大、太活跃，可能还需要时间运行额外数据。例如，对于导航的话，是需要获得一些天气、路况的信息，这是动态的，没有太多学习时间。如果说我们要获得最新新闻，了解股市的最新变化。上图是新的系统，重叠的部分就是我们所预测的真正变革，我认为我们看到的只是真正变革的一个重要信号，但是现在的话这个变革还没有到来。

我们现在的系统只能做一些夺眼球的任务，对我们而言围棋似乎很重要，但对这个社会来说还没有那么重要。我们可以研究对象识别，开发许多好的应用并利用这样的能力帮助我们赚钱。但是在这个中心，我们会产生下一次革命，处理知识和数据的能力将实现飞跃。

IBM的系统由Wason和有两个机器人组成，这三者都无法连接至网络，相当公平。这两个机器人就像我们的人一样长有脑袋，Wason利用大量机器 和存储从维基百科、字典、圣经、历史书上面下载大量的信息，这就是未来。如果我们利用这种技术把行为学习和深度学习的力量结合起来，机器不会和人类一样蠢，在某些方面必定是超级智能的，而且我们的大脑、知识和语言是通过变革不断进化的，知识变革是后来兴起的，如果能将知识变革和行为变革结合起来，就可以构建任何类型的革命性系统并快速复制这种行为，这种系统必定会非常强大。事实上人们已经在进行这项工作，并且不断地在完善各种系统，


人工智能会带来什么？

AI的巨大潜力是在于：
可管理的语义技术；
强大的学习技术；
合适的表示媒介数学模型；我们不能低估它的能力，有些人认为只需提供原始数据就可以了，其他的机器可以帮你做，但是如果不理解图片、视频、语言、语音和内部结构，就不可能做出很好的系统。
高效的大数据管理技术。例如Spark、Flink等；
大量的有意义结构化和非结构化数据。如果综合这些技术，再加上我们的一些方法，我们就可以利用不同的策略将深度学习与大型知识库结合起来。我相信未来会出现更加智能的机器。AI不能取代人类，人工智能无法完成策略规划、创造性设计，不会做研究，也不会做智能管理。但是人工智能够让有创意的人更加的强大。

最后我来概括一下我的预测，到底人工智能会给我们带来什么？

它能激活巨大的经济储备，其他演讲者已提及这一点，当前社会、产业、交通、医疗等领域中存在巨大的资源浪费问题。它能极大地增强人类认知。我们现在只有两只眼睛、两只耳朵，我们的脑袋也很小，我们的大脑很难记住两三本书的内容，但是的机器是能够轻易地记住数百万本书的内容。另外的话我们也不知道其他地方在发生事情，机器可以告诉我们。它能解放异化劳动。借助AI我们可以解放一些从事危险、繁重工作的劳动力，比如上周我在上海的演讲中提及，社会要对未来的变化做好准备，一些低级工作将会被终结。有些工作岗位上的劳动者到了40岁，就开始希望早点退休，像这样的工作就应该消失。

未来其他类型的工作则会产生巨大的需求，这是另一个话题。因为现在社会上的许多工作是需要的是人而不是计算机。事实上我们缺少的并不是劳动力，而是专家，但是我们没有足够的资金来招募这些专家。它能完善服务和知识型社会。它能加速知识变革。它能使我们的生活变得更好、更安全。