---
layout: page
title: 关于
description: 
keywords: Zhang Huan, 8爪鱼作坊
comments: true
menu: 关于
permalink: /about/
---

## Richard

* 一生放荡不羁爱自由

## 联系

* GitHub：[@octopus](https://github.com/octopusy)
* 简书: [@octopus](https://www.jianshu.com/u/aee1aeb16c02)

## Skill Keywords

#### Software Engineer Keywords

<div class="btn-inline">
    {% for keyword in site.skill_software_keywords %}
    <button class="btn btn-outline" type="button">{{ keyword }}</button>
    {% endfor %}
</div>

#### Mobile Developer Keywords

<div class="btn-inline">
    {% for keyword in site.skill_mobile_app_keywords %}
    <button class="btn btn-outline" type="button">{{ keyword }}</button>
    {% endfor %}
</div>

<!--#### Windows Developer Keywords
<div class="btn-inline">
    {% for keyword in site.skill_windows_keywords %}
    <button class="btn btn-outline" type="button">{{ keyword }}</button>
    {% endfor %}
</div>-->
